var mongoose = require('mongoose');
  Schema = mongoose.Schema;

module.exports = mongoose.model('User', {
  email: String,
  pwd: String,
  facebook: {
      fbid:{
          type: String,
          trim: true
      },
      token:{
          type: String
      },
      displayName:{
          type: String
      },
      email:{
          type: String
      },
      profileUrl:{
          type: String
      }
  }
});
