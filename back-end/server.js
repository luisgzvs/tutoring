var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var auth = require('./controllers/auth');
var register = require('./controllers/register');
var message = require('./controllers/message');
var checkAuthenticated = require('./services/checkAuthenticated');
var cors = require('./services/cors');

app.use(bodyParser.json());
app.use(cors);

//Functioon to post a message in DB
app.post('/api/message', message.post);
// Get all messages from DB
app.get('/api/message', message.get);
//Function to save users in mongodb
app.post('/regist/registerUser', register.registerUser);
//Function to checkAuthenticated user
app.post('/api/message',checkAuthenticated, message.post);
//Function to login
app.post('/auth/login', auth.login);

//enlace a la BD si no existe la crea con el nombre del parametro
mongoose.connect("mongodb://localhost:27017/test", function(err, db) {
  if (!err) {
    console.log("we are connected to mongo");
  }
})

var server = app.listen(5000, function() {
  console.log('listening on port ', server.address().port)
})
