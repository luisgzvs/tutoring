var User = require('../models/User'); //double point because we need to go up one directory
var jwt = require('jwt-simple'); // library for token authentication
var moment = require('moment');

module.exports = {
  registerUser: function(req, res) {
    console.log(req.body);

    //function to verify an existing user by email
    User.findOne({
      email: req.body.email
    }, function(err, existingUser) {

      if (existingUser)
        return res.status(409).send({
          message: 'Email is already registered'
        });

      var user = new User(req.body);

      user.save(function(err, result) {
        if (err) {
          res.status(500).send({
            message: err.message
          });
        }
        res.status(200).send({
          token: createToken(result)
        }); //results contain the user we just saved in user.save
      })
    });
  },
  login: function(req, res) {
    console.log(req.body);

    //function to verify an existing user by email
    User.findOne({
      email: req.body.email
    }, function(err, user) {

      if (!user)
        return res.status(409).send({
          message: 'User not found!'
        });

      if (req.body.pwd == user.pwd) {
        console.log(req.body, user.pwd)
        res.send({
          token: createToken(user)
        });
      } else {
        return res.status(401).send({
          message: 'Invalid email or password'
        });
      }
    });
  }
}

function createToken(user) {
  var payload = {
    sub: user._id,
    iat: moment().unix(),
    exp: moment().add(14, 'days').unix()
  };
  return jwt.encode(payload, 'secret'); //in production secret should be more elaborated
}
