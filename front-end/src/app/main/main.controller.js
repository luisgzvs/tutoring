export class MainController {
  constructor ($http) { //constructor de la clase MainController, $http??
    'ngInject';

      this.$http = $http;
      this.getMessages();
      this.$('.dropdown').dropdown();
  }

  getMessages() {
    var vm = this;
      this.$http.get('http://localhost:5000/api/message').then(function(result){
        vm.messages =result.data; // crea la variable messages para enviarla al view con el resultado de data
      });

  }

  postMessage() {
      this.$http.post('http://localhost:5000/api/message', {msg: this.message}); //msg: this.message es el parametro que se pasa para el post
  }

}
