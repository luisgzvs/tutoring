export class AuthController {
  constructor($state, $auth) {
    'ngInject';

    this.$state= $state;
    this.$auth = $auth;
  }

  callRegistview() {
    this.$state.go('regist');
  }

  login() {
    var vm = this;
    vm.errorMessage;

    this.$auth.login(this.login.user).then(function(token) {
      vm.$auth.setToken(token);
    },function(result){
        vm.errorMessage = result.data.message;
    });
  }

}
