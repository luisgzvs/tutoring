export class RegisterController {

  constructor($auth, $state) {
    'ngInject';

    this.$auth = $auth;
    this.$state = $state;
  }

  registerUser() {
    var vm = this;

    this.$auth.signup(this.user).then(function(token) {
      vm.$auth.setToken(token);
    },function(result){
        vm.errorMessage = result.data.message;
    });
  }

  callAuthview() {
    this.$state.go('auth');
  }

}
